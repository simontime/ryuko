require "kemal"
require "logger"
require "signal"

require "./context"
require "./handler"
require "./janitor"
# require "./config_checker"

ctx = RyukoContext.new
ctx.setup

static_headers do |response, filepath, filestat|
  if filepath =~ /\.html$/
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
  end

  response.headers.add("Content-Size", filestat.size.to_s)
end

add_context_storage_type(RyukoContext)
add_context_storage_type(Logger)

before_all "*" do |env|
  env.set "ctx", ctx
  env.set "log", ctx.log
end

error 500 do |env, err|
  {error:   true,
   code:    500,
   message: "#{err}"}.to_json
end

error 404 do |env, err|
  "Path not found."
end

get "/.well-known/ryuko" do |env|
  env.response.status_code = 204
  ""
end

# get "/" do |env|
#  send_file env, "./priv/frontend/build/index.html"
# end

def janitor_loop(ctx : RyukoContext, retries = 0)
  begin
    while true
      ctx.log.debug("running janitor")
      janitor_tick(ctx)
      sleep 30.seconds
    end
  rescue ex
    sleep_sec = Random.rand(2..12)
    max_retries = ENV["JANITOR_MAX_RETRIES"]? || 10
    janitor_retry = retries < max_retries.to_i32
    if janitor_retry
      retry_msg = "retrying in #{sleep_sec}s."
    else
      retry_msg = "STOPPING JANITOR"
    end

    ctx.log.error("janitor error. #{retry_msg} #{ex.inspect_with_backtrace}")

    if janitor_retry
      sleep sleep_sec
      janitor_loop(ctx, retries + 1)
    else
      return
    end
  end
end

Kemal.run do |config|
  ctx.check_port(config)
  spawn do
    janitor_loop(ctx)
  end

  Signal::INT.trap do
    ctx.log.info "shutting down"
    # ctx.db.close
    Kemal.stop
    exit
  end
end
