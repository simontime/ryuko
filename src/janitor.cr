def janitor_tick(ctx)
  begin
    cached_files : Dir = Dir.open(ctx.cache_dir)
  rescue ex : File::NotFoundError
    # temporary dir doesn't exist? we just return and wait for the next tick.
    return
  end

  begin
    cached_files.each do |entry|
      filepath = ctx.cache_dir / entry
      info = File.info(filepath)

      if info.directory?
        next
      end

      now = Time.local
      expiration_time = (ENV["MAX_CACHE_MIN"]? || 30).to_u64

      if now > (info.modification_time + expiration_time.minutes)
        ctx.log.debug("Deleting '#{entry}', too old")
        File.delete(filepath)
      end
    end
  ensure
    cached_files.close
  end
end
