ENV["KEMAL_ENV"] = "test"
ENV["CRYSTAL_ENV"] = "debug"

ENV["NO_NGINX"] = "1"
ENV["ELIXIRE_URL"] = ENV["ELIXIRE_URL"]? || "https://elixi.re"

# TODO remove this and use defaults?
ENV["JANITOR_MAX_RETRIES"] = "0"

require "spec-kemal"
require "../src/ryuko"
