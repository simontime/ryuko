#!/bin/sh

set -o nounset
set -o errexit

mkdir -p ./effects/bin

for effect_source_folder in ./effects/src/*; do
    echo "BUILD : $(basename $effect_source_folder)"
    if [ -f "$effect_source_folder/Makefile" ]; then
        cd "$effect_source_folder"
        make
        cd ../../..
        cp -v $effect_source_folder/bin/* ./effects/bin/
    else
        cp -v $effect_source_folder/* ./effects/bin/
    fi
    echo ""
done
