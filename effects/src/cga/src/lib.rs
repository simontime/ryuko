use delta_e::DE2000;
use image::{Pixel, Rgb};
use ndarray::prelude::*;
use num_traits::cast::NumCast;
use rayon::prelude::*;
use std::fs::File;
use std::io::{prelude::*, BufWriter};
use std::path::Path;

#[derive(Debug)]
pub struct BayerMatrix {
    matrix: Array2<u8>,
}

impl BayerMatrix {
    pub fn new(size: usize) -> Self {
        let mut matrix = Array2::zeros((size, size));
        Self::expand(matrix.view_mut(), 0, 1);
        Self { matrix }
    }

    fn expand(mut matrix: ArrayViewMut2<u8>, value: u8, step: u8) {
        if matrix.nrows() == 1 {
            matrix[[0, 0]] = value;
        } else {
            let half = matrix.nrows() / 2;
            Self::expand(matrix.slice_mut(s![..half, ..half]), value, step * 4);
            Self::expand(matrix.slice_mut(s![half.., half..]), value + step, step * 4);
            Self::expand(
                matrix.slice_mut(s![half.., ..half]),
                value + step * 2,
                step * 4,
            );
            Self::expand(
                matrix.slice_mut(s![..half, half..]),
                value + step * 3,
                step * 4,
            );
        }
    }

    pub fn apply(&self, x: usize, y: usize, pixel: Rgb<u8>) -> Rgb<u8> {
        let x = x % self.matrix.ncols();
        let y = y % self.matrix.nrows();
        let threshold = self.matrix[[x, y]] as i16;
        let offset = (765 * threshold) / (8 * self.matrix.len() as i16);
        pixel.map(|x| {
            let y = x as i16 + offset;
            if y > 255 {
                255
            } else if y < 0 {
                0
            } else {
                y as u8
            }
        })
    }
}

pub fn euclidean_color_distance<T: Pixel>(a: T, b: T) -> u32 {
    a.channels()
        .iter()
        .copied()
        .map(|x| <u32 as NumCast>::from(x).unwrap())
        .zip(
            b.channels()
                .iter()
                .copied()
                .map(|x| <u32 as NumCast>::from(x).unwrap()),
        )
        .map(|(x, y)| {
            if x > y {
                (x - y) * (x - y)
            } else {
                (y - x) * (y - x)
            }
        })
        .sum()
}

pub fn de2000_color_distance(a: Rgb<u8>, b: Rgb<u8>) -> u32 {
    DE2000::from_rgb(&a.0, &b.0).abs().to_bits()
}

fn closest_color<P, D>(color: P, palette: &[P], mut color_distance: D) -> Option<usize>
where
    P: Pixel,
    D: FnMut(P, P) -> u32,
{
    palette
        .iter()
        .enumerate()
        .min_by_key(|&(_, &x)| color_distance(x, color))
        .map(|(i, _)| i)
}

pub fn dither<'a, I, D>(
    image: I,
    matrix: &'a BayerMatrix,
    palette: &'a [Rgb<u8>],
    color_distance: D,
) -> impl ParallelIterator<Item = u8> + 'a
where
    I: ParallelIterator<Item = (u32, u32, Rgb<u8>)> + 'a,
    D: Fn(Rgb<u8>, Rgb<u8>) -> u32 + Send + Sync + 'a,
{
    image.map(move |(x, y, p)| {
        if let Some(idx) = palette.iter().position(|&x| x == p) {
            idx as u8
        } else {
            let bayer = matrix.apply(x as _, y as _, p);
            closest_color(bayer, palette, &color_distance).unwrap() as u8
        }
    })
}

pub fn save_paletted_png(
    path: &Path,
    width: u32,
    height: u32,
    data: &[u8],
    palette: &[Rgb<u8>],
) -> anyhow::Result<()> {
    let file = File::create(path)?;
    let mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(&mut w, width, height);

    let palette = palette.iter().flat_map(Rgb::channels).copied().collect();
    encoder.set_palette(palette);
    encoder.set_color(png::ColorType::Indexed);

    let mut writer = encoder.write_header()?;
    writer.write_image_data(data)?;
    drop(writer);

    w.flush()?;

    Ok(())
}

pub const WIN16_PALETTE: &[Rgb<u8>] = &[
    Rgb([0x00, 0x00, 0x00]),
    Rgb([0x80, 0x80, 0x80]),
    Rgb([0x80, 0x00, 0x00]),
    Rgb([0xFF, 0x00, 0x00]),
    Rgb([0x00, 0x80, 0x00]),
    Rgb([0x00, 0xFF, 0x00]),
    Rgb([0x80, 0x80, 0x00]),
    Rgb([0xFF, 0xFF, 0x00]),
    Rgb([0x00, 0x00, 0x80]),
    Rgb([0x00, 0x00, 0xFF]),
    Rgb([0x80, 0x00, 0x80]),
    Rgb([0xFF, 0x00, 0xFF]),
    Rgb([0x00, 0x80, 0x80]),
    Rgb([0x00, 0xFF, 0xFF]),
    Rgb([0xC0, 0xC0, 0xC0]),
    Rgb([0xFF, 0xFF, 0xFF]),
];
