use anyhow::{anyhow, Result};
use cga::*;
use image::{imageops::FilterType, *};
use rayon::prelude::*;
use std::env::args_os;
use std::path::PathBuf;
use whaterror::whaterror;

#[whaterror(|err| eprintln!("Error: {}", err))]
fn main() -> Result<()> {
    let matrix = BayerMatrix::new(8);

    let path: PathBuf = args_os()
        .nth(1)
        .ok_or_else(|| anyhow!("Missing path!"))?
        .into();
    let image = open(&path)?;

    let scaled = image.resize(160, 100, FilterType::Triangle).into_rgb8();

    let dithered = dither(
        scaled.par_chunks(3).enumerate().map(|(i, s)| {
            (
                i as u32 % scaled.width(),
                i as u32 / scaled.width(),
                *Rgb::from_slice(s),
            )
        }),
        &matrix,
        WIN16_PALETTE,
        euclidean_color_distance,
    );

    if matches!(path.extension(), Some(ext) if ext == "png") {
        save_paletted_png(
            &path,
            scaled.width(),
            scaled.height(),
            &dithered.collect::<Vec<_>>(),
            WIN16_PALETTE,
        )?;
    } else {
        let dithered_buf = dithered
            .flat_map_iter(|x| WIN16_PALETTE[x as usize].channels())
            .copied()
            .collect();
        let dithered_img =
            RgbImage::from_vec(scaled.width(), scaled.height(), dithered_buf).unwrap();
        dithered_img.save(&path)?;
    }

    Ok(())
}
