use anyhow::{anyhow, Result};
use image::*;
use std::env::args;
use whaterror::whaterror;

#[whaterror(|err| eprintln!("Error: {}", err))]
fn main() -> Result<()> {
    let path = args().nth(1).ok_or_else(|| anyhow!("Missing path!"))?;
    let mut image = open(&path)?.to_luma8();

    macintosh::dither(&mut image);

    image.save(&path)?;

    Ok(())
}
