use image::*;
use std::cell::Cell;
use std::ops::{Deref, DerefMut};

pub fn dither<C>(image: &mut ImageBuffer<Luma<u8>, C>)
where
    C: Deref<Target = [u8]> + DerefMut,
{
    let w = image.width() as usize;
    let h = image.height() as usize;
    let buf = Cell::as_slice_of_cells(Cell::from_mut(image));
    for (i, cell) in buf.iter().enumerate() {
        let x = i % w;
        let y = i / w;
        let v = cell.get();
        let quantized = if v > 127 { 255 } else { 0 };
        cell.set(quantized);
        let error = quantized as i16 - v as i16;
        let divided = error / 8;
        for &(x, y) in &[
            (x + 1, y),
            (x + 2, y),
            (x - 1, y + 1),
            (x, y + 1),
            (x + 1, y + 1),
            (x, y + 2),
        ] {
            if x >= w || y >= h {
                continue;
            }
            let i = x + y * w;
            let cell = &buf[i];
            let v = cell.get();
            let diffused = v as i16 - divided;
            cell.set(if diffused > 255 {
                255
            } else if diffused < 0 {
                0
            } else {
                diffused as u8
            });
        }
    }
}
